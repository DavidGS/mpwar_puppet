class{'apache':}

apache::vhost { 'myMpwar.dev':
      port          => '80',
      docroot       => '/var/web',
}

class { 'mysql':
  root_password => 'auto',
}
mysql::grant { 'mympwar':
  mysql_privileges => 'ALL',
  mysql_password => 'pwd',
  mysql_db => 'mympwar',
  mysql_user => 'mpwar_user',
  mysql_host => 'localhost',
}

$yum_repo = 'remi-php55'
include ::yum::repo::remi_php55

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

include memcached
